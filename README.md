# vrtel.dev web project

Use `entr` tool to watch for changes of the `src` directory and run rendering script.

Script outputs into `public` directory.

`find src | entr -c bash scripts/render.sh`

Using npm package `serve` to serve files locally.
