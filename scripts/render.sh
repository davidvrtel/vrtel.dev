#!/bin/bash

r_output_dir=public/ #used
r_page_dirs=$(find src/pages -type d -printf '%P\n') #used

r_pages=$(find src/pages -name "index.html" -printf '%P\n') #used
r_page_src=src/pages/ #used

r_styles_src=src/styles #used
r_styles_dir=${r_output_dir}styles #used

r_base_layout=$(<src/layout.html) #used
r_tmp_html_file="" #used
r_tmp_result="" #used
r_tmp_layout="" #used

# prepare output directory
mkdir $r_output_dir &>/dev/null
rm -rf $r_output_dir*

# to specify base tag href, supply base as first argument
# must escape the forward slashes, such as "https:\/\/vrtel.dev"
[ "$1" ] && echo -e "\nBase supplied: $1\n"

# set base tag if supplied as first script argument
if [ ! -z "$1" ]
then
    r_base_layout=$(sed -e "s/PLACEHOLDER_BASE_HREF/$1/g" <<< $r_base_layout)
else
    r_base_layout=$(sed -e "/PLACEHOLDER_BASE_HREF/d" <<< $r_base_layout)
fi

# create page directories
for dr in $r_page_dirs
do
    mkdir -p $r_output_dir$dr
done

# render pages
echo "Pages found:"
for html in $r_pages
do
    echo -e "\t$html"

    # load main content of page
    r_tmp_html_file=$(<$r_page_src$html)
    r_tmp_title=$(sed -n 's/^PLACEHOLDER_TITLE=//p' <<< $r_tmp_html_file)

    # replace title from variable on top of page file
    r_tmp_html_file=$(sed -e '/^PLACEHOLDER_TITLE=/d' <<< $r_tmp_html_file)
    r_tmp_layout=$(sed -e "s/PLACEHOLDER_TITLE/$r_tmp_title/" <<< $r_base_layout)

    # sed can't easily replace string by string from multiline variable
    # take lines before the placeholder, concat the page file and then concat the rest of layout
    r_tmp_result=$(sed '/PLACEHOLDER_MAIN_CONTENT/Q' <<< $r_tmp_layout)
    r_tmp_result+="$r_tmp_html_file"
    r_tmp_result+=$(sed -n '/PLACEHOLDER_MAIN_CONTENT/,$p' <<< $r_tmp_layout | sed '1d')

    # write the result into output directory
    echo "$r_tmp_result" > $r_output_dir$html
done

echo -e "\nCopying styles"
cp -RT $r_styles_src $r_styles_dir

echo -e "\nRendered at $(date -Is)"
